﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using SpecflowSudent.Pages;
using specflowSudent.Widgets;
using TechTalk.SpecFlow;
using TestProject;

namespace specflowSudent
{
    [Binding]
    class beadandoTestSteps 
    {
        
        public static string actualPage = "actualPage";

        [Given(@"I open the survey")]
        public void GivenIOpenTheSurvey()
        {
            var driver = (IWebDriver) ScenarioContext.Current[BDDTestBase.webDriver];
            var pageToTest = SearchPage.Navigate(driver, "https://www.surveymonkey.com/r/FNRVK86");
            ScenarioContext.Current.Add(actualPage,pageToTest);
        }

        
        [When(@"I answer to questions on the first page")]
        public void WhenIAnswerToQuestionsOnTheFirstPage()
        {
            var pageToTest = ScenarioContext.Current.Get<SearchPage>(actualPage);
            var searchWidget= pageToTest.GetSurveySearchWidget();
            //firstTask
            searchWidget.SetRadioButton("1");
            searchWidget.SetRadioButton("7");
            searchWidget.SetComboBox("Page Object");
            searchWidget.SetRatingContainer();

        }

        [When(@"I proceed to the next page")]
        public void WhenIProceedToTheNextPage()
        {
            var pageToTest = ScenarioContext.Current.Get<SearchPage>(actualPage);
            pageToTest.GetSurveySearchWidget().PressButtonOnFirstPage();
        }

        [When(@"I answer to questions on the second page")]
        public void WhenIAnswerToQuestionsOnTheSecondPage()
        {
            ScenarioContext.Current.Pending();
        }

        [When(@"I send my answers in")]
        public void WhenISendMyAnswersIn()
        {
            ScenarioContext.Current.Pending();
        }

        [Then(@"The result should contain Thank you!")]
        public void ThenTheResultShouldContainThankYou()
        {
            ScenarioContext.Current.Pending();
        }

    }
}
