﻿@feature_tag
Feature: BDDTest elvira

@basic
Scenario: Create a search between two cities
	Given I open elvira mav-start page
	# 1. generate missing step definitions
	# 2. implement the steps where you can use the predefined page objects
	When I create a search from Székesfehérvár to Sopron
	And I submit the search from
	Then the search result title should contain Székesfehérvár and Sopron

@tables
Scenario: Create a search between two cities using tables
	Given I open elvira mav-start page
	When I create a search with the following parameters
	| from    | to       | via            |
	| Szolnok | Debrecen | Hajdúszoboszló |
	And I submit the search from
	Then the search result title should contain the following city names
	| cities         |
	| Szolnok        |
	| Debrecen       |
	| Hajdúszoboszló |

