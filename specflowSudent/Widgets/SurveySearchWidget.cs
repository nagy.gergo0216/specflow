﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SpecflowSudent.Pages;

namespace specflowSudent.Widgets
{
    class SurveySearchWidget : BasePage
    {
        public SurveySearchWidget(IWebDriver webDriver) : base(webDriver)
        {
        }


        private ReadOnlyCollection<IWebElement> RadioButtons =>
            Driver.FindElements(By.ClassName("radio-button-container"));

        private IWebElement ComboBox => Driver.FindElement(By.Id("373480133"));

        private List<IWebElement> ratingElements;

        private void SetListForRatingElements()
        {
            ratingElements = new List<IWebElement>();
            ratingElements.Add(Driver.FindElement(By.Id("373480134_2479050988")));
            ratingElements.Add(Driver.FindElement(By.Id("373480134_2479050997")));
            ratingElements.Add(Driver.FindElement(By.Id("373480134_2479050998")));
            ratingElements.Add(Driver.FindElement(By.Id("373480134_2479050999")));
            ratingElements.Add(Driver.FindElement(By.Id("373480134_2479051000")));

            ratingContainer = ratingElements.AsReadOnly();
        }

        private List<IWebElement> radioButtonElements;
        private void SetRadioButtonElements()
        {
            radioButtonElements = new List<IWebElement>();
            radioButtonElements.Add(Driver.FindElement(By.CssSelector("div.answer-option-cell:nth-child(1) > div:nth-child(1) > label:nth-child(2) > span:nth-child(2)")));
            radioButtonElements.Add(Driver.FindElement(By.CssSelector("div.answer-option-cell:nth-child(2) > div:nth-child(1) > label:nth-child(2) > span:nth-child(2)")));
            radioButtonElements.Add(Driver.FindElement(By.CssSelector("div.answer-option-cell:nth-child(3) > div:nth-child(1) > label:nth-child(2) > span:nth-child(2)")));
            radioButtonElements.Add(Driver.FindElement(By.CssSelector("div.answer-option-cell:nth-child(4) > div:nth-child(1) > label:nth-child(2) > span:nth-child(2)")));
            radioButtonElements.Add(Driver.FindElement(By.CssSelector("div.answer-option-cell:nth-child(5) > div:nth-child(1) > label:nth-child(2) > span:nth-child(2)")));

            radioButtonsOnSecondPage = radioButtonElements.AsReadOnly();

        }

        private ReadOnlyCollection<IWebElement> ratingContainer;

        private IWebElement PressButtonOnFirstPageElement => Driver.FindElement(By.CssSelector(".btn"));

        private ReadOnlyCollection<IWebElement> radioButtonsOnSecondPage;

        private IWebElement MessageBox => Driver.FindElement(By.Id("373480136"));

        private IWebElement PressButtonOnSecondPageElement => Driver.FindElement(By.CssSelector("button.btn:nth-child(2)"));

        public string Title => Driver.Title;

        private IWebElement TestElek => Driver.FindElement(By.Id("373480135_2479050991"));

        public void SetRadioButton(string id)
        {
            RadioButtons[int.Parse(id)].Click();
        }

        public void SetComboBox(string text)
        {
            ComboBox.Click();
            new SelectElement(ComboBox).SelectByText(text);
        }

        public void SetRatingContainer()
        {
            SetListForRatingElements();
            ratingContainer[0].Click();
            new SelectElement(ratingContainer[0]).SelectByIndex(5);
            ratingContainer[1].Click();
            new SelectElement(ratingContainer[1]).SelectByIndex(4);
            ratingContainer[2].Click();
            new SelectElement(ratingContainer[2]).SelectByIndex(2);
            ratingContainer[3].Click();
            new SelectElement(ratingContainer[3]).SelectByIndex(3);
            ratingContainer[4].Click();
            new SelectElement(ratingContainer[4]).SelectByIndex(1);
        }

        public void PressButtonOnFirstPage()
        {
            PressButtonOnFirstPageElement.Click();
        }

        public void SetRadioButtonOnSecondPage(string id)
        {
            SetRadioButtonElements();
            radioButtonsOnSecondPage[int.Parse(id)].Click();

        }

        public void SetMessageBox(string message)
        {
            MessageBox.SendKeys(message);
        }

        public void PressButtonOnSecondPage()
        {
            PressButtonOnSecondPageElement.Click();
        }

    }
}
