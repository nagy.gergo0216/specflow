﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using SpecflowSudent.Pages;
using TechTalk.SpecFlow;

namespace TestProject
{
    [Binding]
    public class BDDTestBase
    {
        public static string webDriver = "driver";

        [BeforeScenario]
        public void BeforeScenario()
        {
            ChromeOptions options = new ChromeOptions();
            options.AddArguments("--lang=hu");
            var driver = new ChromeDriver(options);
            ScenarioContext.Current.Add(webDriver, driver);
            
            
        }

        [AfterScenario]
        public void AfterScenario()
        {
            var driver = ScenarioContext.Current.Get<IWebDriver>(webDriver); ;
            driver.Quit();
        }
    }
}
