﻿using System.Drawing;
using NUnit.Framework;
using OpenQA.Selenium;
using SpecflowSudent.Pages;
using SpecflowSudent.Widgets;
using TechTalk.SpecFlow;
using TestProject;

namespace SpecflowSudent
{
    [Binding]
    class BDDTestSteps
    {
        public static string actualPage = "actualPage";

        [Given(@"I open elvira mav-start page")]
        public void GivenIOpenElviraMav_StartPage()
        {
            var driver = (IWebDriver)ScenarioContext.Current[BDDTestBase.webDriver];
            var pageToTest = SearchPage.Navigate(driver, "http://elvira.mav-start.hu/elvira.dll/xslvzs/index?language=1");
            ScenarioContext.Current.Add(actualPage,pageToTest);
        }


        [When(@"I create a search from Székesfehérvár to Sopron")]
        public void WhenICreateASearchFromSzekesfehervarToSopron()
        {
            var pageToTest = ScenarioContext.Current.Get<SearchPage>(actualPage);
            pageToTest.GetSearchWidget().SearchForRoute("Székesfehérvár","Sopron");
        }

        [When(@"I submit the search from")]
        public void WhenISubmitTheSearchFrom()
        {
            var pageToTest = ScenarioContext.Current.Get<SearchPage>(actualPage);
            var clickToButton = pageToTest.GetSearchWidget().ClickTimetableButton();
            ScenarioContext.Current[actualPage] = clickToButton;
        }

        [Then(@"the search result title should contain Székesfehérvár and Sopron")]
        public void ThenTheSearchResultTitleShouldContainSzekesfehervarAndSopron()
        {
            var pageToTest = ScenarioContext.Current.Get<SearchPage>(actualPage);
            var result = pageToTest.GetResultWidget().ReturnResultTitle();
            StringAssert.Contains("Székesfehérvár",result);
            StringAssert.Contains("Sopron",result);
        }

        [When(@"I create a search with the following parameters")]
        public void WhenICreateASearchWithTheFollowingParameters(Table table)
        {
        }

        [Then(@"the search result title should contain the following city names")]
        public void ThenTheSearchResultTitleShouldContainTheFollowingCityNames(Table table)
        {
        }

    }
}
