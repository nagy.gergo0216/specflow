﻿using OpenQA.Selenium;

namespace SpecflowSudent.Pages
{
    class BasePage
    {
        protected IWebDriver Driver;

        public BasePage(IWebDriver webDriver)
        {
            Driver = webDriver;
        }
    }
}
