﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using specflowSudent.Widgets;
using SpecflowSudent.Widgets;

namespace SpecflowSudent.Pages
{
    class SearchPage : BasePage
    {
        public SearchPage(IWebDriver webDriver) : base(webDriver)
        {
        }

        public static SearchPage Navigate(IWebDriver webDriver,string url)
        {
            webDriver.Navigate().GoToUrl(url);
            return new SearchPage(webDriver);
        }

        public SearchWidget GetSearchWidget()
        {
            return new SearchWidget(Driver);
        }

        public ResultWidget GetResultWidget()
        {
            return new ResultWidget(Driver);
        }

        public SurveySearchWidget GetSurveySearchWidget()
        {
            return new SurveySearchWidget(Driver);
        }
    }
}
